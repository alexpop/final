"""Methods for processing sound (ampli, reduce, extend)"""

import config

import array

min_amp = - config.max_amp
max_amp = config.max_amp

def ampli(sound, factor: float):
    """Metodo para amplificar el sonido"""
    for nsample in range(len(sound)):
        sample = int(sound[nsample] * factor)
        if sample > max_amp:
            sample = max_amp
        elif sample < min_amp:
            sample = min_amp
        else:
            sample = sample

        sound[nsample] = sample
    return sound

def reduce(sound, factor: int):
    """Metodo para reducir muestras de sonido"""
    sound_red = array.array('h')
    for nsample in range(0, len(sound), factor):
        sound_red += sound[nsample:nsample+(factor-1)]

    return sound_red

def extend(sound, factor: int):
    """Metodo para extender muestras de sonido"""
    sound_ext = array.array('h')
    for nsample in range(0, len(sound), factor):
        if (nsample + factor) < len(sound):
            sound_ext += sound[nsample:nsample + factor]
            sample_new = (sound[nsample+(factor - 1)] + sound[nsample + factor]) / 2
            sound_ext += array.array('h', [int(sample_new)])
        else:
            sound_ext += sound[nsample:]

    return sound_ext

